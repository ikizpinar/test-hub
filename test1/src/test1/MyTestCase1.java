package test1;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class MyTestCase1 {
	private static WebDriver browser = null;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		System.setProperty("webdriver.chrome.driver", "c:\\Users\\fikret ikizp�nar\\chromedriver.exe");
		browser = new ChromeDriver();
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		browser.close();
		browser = null;
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {

	}

	@Test
	public void main_page_test() {
		try {
			browser.get("https://www.techcrunch.com");
			WebElement main = browser.findElement(By.xpath("//a[@href='/']"));
			assertTrue(main.isDisplayed());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void latest_news_author_test() {
		try {
			browser.get("https://www.techcrunch.com");
			WebElement latest = browser.findElement(By.className("river--homepage"));
			assertFalse(latest == null);

			List<WebElement> news = latest.findElements(By.tagName("article"));

			for (WebElement article : news) {
				WebElement author = article.findElement(By.className("river-byline__authors"));
				// If any article has an author but name is empty, test fails.
				assertTrue((author != null && !article.getText().isEmpty()));
			}
			System.out.println("All articles has authors, pass!");
		} catch (NoSuchElementException e) {
			// If any article has not an author, test fails.
			System.out.println("The article has no author, fail!");
			assertFalse(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void latest_news_image_test() {
		try {
			browser.get("https://www.techcrunch.com");
			WebElement latest = browser.findElement(By.className("river--homepage"));
			assertFalse(latest == null);

			List<WebElement> articles = latest.findElements(By.tagName("article"));
			for (WebElement article : articles) {
				WebElement media =  article.findElement(By.tagName("img"));
				// If any article has not an image, test fails.
				assertTrue((media != null));
			}
			System.out.println("All articles has images, pass!");
		} catch (NoSuchElementException e) {
			// If any article has not an image, test fails.
			System.out.println("Article has no image, fail!");
			assertFalse(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void click_news() {
		try {
			browser.get("https://www.techcrunch.com");
			WebElement latest = browser.findElement(By.className("river--homepage"));
			assertFalse(latest == null);

			WebElement article = latest.findElement(By.tagName("article"));
			article.click();

			WebElement title = browser.findElement(By.className("article__title"));

			assertTrue(title.getText().trim().equals(browser.getTitle().substring(0, browser.getTitle().indexOf("|")).trim()));
			System.out.println("The browser title same is the news title.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void news_content_verify() {
		try {
			browser.get("https://www.techcrunch.com");
			WebElement latest = browser.findElement(By.className("river--homepage"));
			assertFalse(latest == null);

			WebElement article = latest.findElement(By.tagName("article"));
			article.click();

			WebElement content = browser.findElement(By.className("article-content"));

			assertTrue(content != null && !content.getText().isEmpty());
			System.out.println("The news has content.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
